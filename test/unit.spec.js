var chai = require('chai');
var assert = chai.assert;
var search = require('../search');
var express = require('express');

const request = require('supertest');


describe('getDistance()', () => {
  it('should calculate 0 distance', () => {
        var distance = search.getDistance(1,-0.5,1,-0.5);
        assert.equal(distance, 0);
    });

    it('should calculate some distance', () => {
        var distance = search.getDistance(1,-0.5,1,-0.56);
        assert.equal(distance, 6678);
    });

    it('should calculate increasing distance', () => {
        var distance1 = search.getDistance(1,-0.5,1,-0.55);
        var distance2 = search.getDistance(1,-0.45,1,-0.55);
        assert.isAbove(distance2, distance1);
    });

    it('should not calculate and throw error', () => {
    try {
      expect(search.getDistance('a',-0.5,1,-0.55)).not.to.throw(RangeError);
    } catch (err) {
      assert.exists(err);
    }
  });
});

describe('searchItems()', () => {
  it('should get 20 items for Canon with distance', (done) => {
    var results = search.searchItems('Canon', 1.23, -0.9, (results) => {
      assert.equal(results.length, 20);
      assert.isDefined(results[0].distance);
      assert.equal(results[0].item_name, 'Carl Zeiss Distagon 25mm f2 Lens Canon ZE Fit with a Zeiss U');
      assert.equal(results[0].lat, 50.4151344);
      assert.equal(results[0].lng, -5.09094858);
      assert.equal(results[0].item_url, 'plymouth/hire-carl-zeiss-distagon-25mm-f2-lens-canon-ze-fit-with-a-zeiss-u-52165004');
      assert.equal(results[0].img_urls, '["carl-zeiss-distagon-25mm-f2-lens-canon-ze-fit-with-a-zeiss-u-37270977.jpg"]');
      done();
    })
  });

  it('should get 20 items for Canon without distance', (done) => {
    var results = search.searchItems('Canon', null, null, (results) => {
      assert.equal(results.length, 20);
      assert.isUndefined(results[0].distance);
      assert.equal(results[0].item_name, 'Canon EF 24-70mm f2.8 L II USM Lens');
      assert.equal(results[0].lat, 50.4152985);
      assert.equal(results[0].lng, -5.09094);
      assert.equal(results[0].item_url, 'plymouth/hire-canon-ef-2470mm-f28-l-ii-usm-lens-04184457');
      assert.equal(results[0].img_urls, '["canon-ef-2470mm-f28-l-ii-usm-lens-65815757.png"]');
      done();
    })
  });
});

//Failing
// describe('GET /search', () => {
//   it('respond with json - sucessful', (done) => {
//     request(app)
//       .get('/search?search_term=Canon')
//       //.set('Accept', 'application/json')
//       .expect(200)
//       .end( (err, res) => {
//         if (err) return done(err);
//         done();
//       });
//   });

//   it('fails validation', (done) => {
//     request(app)
//       .get('/search')
//       //.set('Accept', 'application/json')
//       .expect(400)
//       .end( (err, res) => {
//         if (err) return done(err);
//         done();
//       });
//   });
// });



