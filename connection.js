const sqlite3 = require('sqlite3').verbose();

module.exports = {
  openConnection: () => {
    var db = new sqlite3.Database('fatlama.sqlite3', (err) => {
      if (err) {
        return console.error(err.message);
      }
    });
  return db
  },

  closeConnection: (db) => {
    db.close((err) => {
      if (err) {
        return console.error(err.message);
      }
    });
  }
}
