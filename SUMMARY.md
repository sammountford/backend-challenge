# Challenge Summary

## Language
I'm most comfortable with PHP as I'm currently using it daily. I haven't used node recently and havent done any testing with Node.js before so thought I'd give it a go using express with ES6.  

## Assumptions and Caveats
For the test I'm going to work within the limits of sqlite rather than change to another storage mechanism. In that case I'm limited by not being able to use the square root funtion which would make searching by distance much easier using a query similar to the one listed below. 

```SELECT lat, lng, SQRT(POW(69.1 * (lat - [startlat]), 2) +POW(69.1 * ([startlng] - lng) * COS(lat / 57.3), 2)) AS distance
FROM items 
where item_name LIKE '%search_term%'
HAVING distance < 25 ORDER BY distance;```

I'm also unable to easily implement fuzzy searching (there is an extension for sqlite but I'd rather provide a portable solution). 

If no search term is given then a validation error should be returned
http://localhost:3000/search

If latitude OR longitude is not provided, only the search term is used for results
http://localhost:3000/search?searchTerm=Canon

If all available inputs are provided the results include the distance and are ordered by distance
http://localhost:3000/search?searchTerm=Canon&lat=51.5113983&lng=-0.142858997


## Code
In terms of weighting, since im not using fuzzy searching or filtering by distance from the source I'm going to;
* grab 100 items searching for the search terms, 
* calculate the distance, 
* order by the distance 
* return the top 20. 

Not the best or most scalable solution but I'm sticking to SQLlite. Otherwise I'd weight full matches over partial matches.

Rate limiting is implemented

## Improvements
There doesn't seem to be an easy way to use Levenshtein string distance in Sqlite searches. I would like to find a way to implement fuzzy searching allowing results like 'Can0n' to return results for 'Canon' (like it does on the current site). I looked into calculating the string matching in javascript, filtering the results further after retreiving them from the database, but it wouldn't be a very scalable solution in my opinion. Mongo or elastic search might be better in this case.
I haven't written tests for node before so have done what I can before running out of time but did run out of time for validation. I can definitley improve the structure of this code! 

## Setup
npm install

node app.js

npm test

