
var search = require('./search');
var express = require('express');
var app = express();
var RateLimit = require('express-rate-limit');

var apiLimiter = new RateLimit({
  windowMs: 1*60*1000, // 1 minutes
  max: 100,
  delayMs: 0, // disabled
  message: "Too many searches from this IP, please try again in 60 seconds"
});
 
app.use('/search/', apiLimiter);
 
app.get('/search', (req, res) => {
  var searchTerm = req.query.searchTerm
  var lat = req.query.lat
  var lng = req.query.lng
  search.searchItems(searchTerm, lat, lng, (err, results) => {
    if(err) {
      console.error(err)
      res.send(err);
    } else {
      res.json(results);
    } 
  })
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});


