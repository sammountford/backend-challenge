var connection = require('./connection');
var geolib = require('geolib');

module.exports = {

  searchItems: (term, lat, lng, callback) => {

    db = connection.openConnection()
    var results = []

    db.serialize(() => {
      db.all(`select * from items where item_name LIKE '%${term}%' limit 100`, (err, rows) => {
        if (err) {
          console.error(err.message);
        } else {
          rows.forEach((i) => {
            item = {}
            item.item_name = i.item_name
            item.lat = i.lat
            item.lng = i.lng
            item.item_url = i.item_url
            item.img_urls = i.img_urls
            if(lat != undefined && lng != undefined) {
              item.distance = getDistance(i.lat, i.lng, lat, lng)
            }
            results.push(item)
          });
          results.sort((a, b) => {
              return a.distance - b.distance;
          });
          results = results.slice(0,20)
          callback(results)
        }  
      });
    });

    connection.closeConnection(db)
  },

  getDistance
}

function getDistance(lat, lng, orig_lat, orig_lng,){
  var distance = geolib.getDistance(
      {latitude: lat, longitude: lng},
      {latitude: orig_lat, longitude: orig_lng}
  );
  return distance
}
